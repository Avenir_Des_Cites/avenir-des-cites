<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200805164114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE accueil (id INT AUTO_INCREMENT NOT NULL, logo VARCHAR(255) DEFAULT NULL, encart_titre VARCHAR(255) NOT NULL, encart_text LONGTEXT NOT NULL, siege_social LONGTEXT NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE equipe_educative CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE events CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE partenaires CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE social_network CHANGE updated_at updated_at DATETIME NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE accueil');
        $this->addSql('ALTER TABLE equipe_educative CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE events CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE partenaires CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE social_network CHANGE updated_at updated_at DATETIME DEFAULT NULL');
    }
}
