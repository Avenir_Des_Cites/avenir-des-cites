<?php

namespace App\Controller\Admin;

use App\Entity\Accueil;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\EasyAdminBundle;

class AccueilCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Accueil::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('encartTitre', 'Titre'),
            ImageField::new('logoFile')
            ->setFormType(VichImageType::class)
            ->setLabel('Logo')
            ->hideOnIndex(),
            TextareaField::new('encartText', 'Texte'),
            TextEditorField::new('siegeSocial', 'Siège social'),
            // DateField::new('updatedAt', 'Mis à jour le')
            // ->hideOnForm()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
    return $actions
        // ...
        // this will forbid to create or delete entities in the backend
        ->disable(Action::NEW, Action::DELETE);
    }
   }

