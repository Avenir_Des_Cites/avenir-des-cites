<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use \DateTime;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ArticleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular("un article")
            ->setPageTitle('index', 'Articles')
        ;
    }

    // public function configureAssets(Assets $assets): Assets
    // {
    //     return $assets
    //         // the argument of these methods is passed to the asset() Twig function
    //         // CSS assets are added just before the closing </head> element
    //         // and JS assets are added just before the closing </body> element
    //         ->addCssFile('build/admin.css');
    // }

    public function configureFields(string $pageName): iterable
{
    return [
        TextField::new('titre', 'Titre'),
        TextareaField::new('resume', 'Résumé'),
        TextEditorField::new('corps', 'Corps'),
        DateField::new('publication', 'Date de Publication')
        ->onlyWhenCreating(),
        ImageField::new('illuFile')
        ->setFormType(VichImageType::class)
        ->setLabel('Illustration')
        ->hideOnIndex()
    ];
}

}
