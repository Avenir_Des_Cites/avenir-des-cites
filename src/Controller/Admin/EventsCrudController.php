<?php

namespace App\Controller\Admin;

use App\Entity\Events;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;


class EventsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Events::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('un nouvel évènement')
            // the visible title at the top of the page and the content of the <title> element
            // it can include these placeholders: %entity_id%, %entity_label_singular%, %entity_label_plural%
            ->setPageTitle('index', 'Évènements')
            ->setPageTitle('new', 'Évènement')
            ->setPageTitle('edit', 'Évènement')
        ;
    }
    public function configureFields(string $pageName): iterable
    {
        return [
            //affiche l'ID mais empêche sa modification
            // IdField::new('id')->hideOnForm(),
            // permet de définir les champs d'administration et leur label en mode édition et création
            TextField::new('name', 'Nom'),
            TextareaField::new('description'),
            DateField::new('date'),
            TextField::new('place', 'Lieu'),
            TimeField::new('time', 'Horaires'),
            ImageField::new('illuFile')
            ->setFormType((VichImageType::class))
            ->setLabel('Illustration'),
            DateField::new('updatedAt', 'Mise à jour le')
            ->HideOnForm()
        ];
    }
}
